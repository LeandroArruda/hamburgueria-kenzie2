import {Component} from 'react'
import Product from './Product'

class MenuContainer extends Component {
    render() {
        return(
            <div>
                {this.props.show && this.props.array.map(e => 
                <li>
                    <Product obj={e}/>
                    {this.props.haveButton &&  <button onClick={this.props.click} id={e.id}>Adicionar</button>  }
                </li>)}
            </div>
        )
    }
}

export default MenuContainer